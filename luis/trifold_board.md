# Tri-fold board

## Story

## Process and Learning

* Brainstorming ideas.
* Review my favorite games.
* Sketch website design.
* Define game categories.
* Prepare questionnaire for 8th graders.
* Learn basic web programming.

## Results

* Learned how websites can be coded.
* Learned more about other types of videogames.
* Learned how important technology is.

Visit the project results at http://videogamesamong8thgraders.herokuapp.com


